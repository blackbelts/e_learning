# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
from odoo.exceptions import ValidationError


class e_learning(models.Model):
    _name = 'comp.learning'
    _rec_name='competency_id'
    # name = fields.Many2one('performance.framework',string='Performance Framework',required=True, domain="[('framework_type','=','Competency')]")
    competency_id = fields.Char(string="Competency ID", required=True,default=lambda self: self.env['ir.sequence'].next_by_code('competency'),readonly=True )
    job_position = fields.Many2one(comodel_name="hr.job", string="Job Position", required=False, )
    competencies_ids = fields.One2many(comodel_name="competencies.details", inverse_name="competency_id", string="", required=False, )
    year = fields.Selection([(num, str(num)) for num in range(2018, (datetime.now().year)+5 )], 'Year',required=False)
    details_ids = fields.One2many(comodel_name="comp.details", inverse_name="competency_id", string="", required=False, )
    department_name = fields.Many2one(comodel_name="hr.department", string="Department", required=False,
                                      related='job_position.department_id', store=True)
    @api.constrains('year')
    def _constrain_comp(self):
        object = self.env['comp.learning'].search(
            [('job_position', '=', self.job_position.id),('year', '=', self.year), ('id', '!=', self.id)])
        if object:
            raise ValidationError('You Have Another Job Analysis Setup in the same year and the same type !')

    @api.constrains('competencies_ids')
    def _constrain_tcompetencies_ids(self):
        total = 0
        for x in self.competencies_ids:
            total += x.weight
        if self.competencies_ids and total != 100:
            raise ValidationError('Total Weights Must be equal 100 !')


class NewModule(models.Model):
    _name = 'competencies.details'
    competency_id = fields.Many2one(comodel_name="comp.learning", string="", required=False, )
    skills_id = fields.Many2one(comodel_name="skill.setup", string="Skill", required=False, )
    description = fields.Text(related='skills_id.description', store=True)
    mastery_level = fields.Selection(string="Mastery Level", selection=[('Level 1', 'Level 1'), ('Level 2', 'Level 2'),('Level 3', 'Level 3') ], required=False,
                                      related='skills_id.mastery_level', store=True)
    weight = fields.Float(string="Weight %",  required=False, )
class skills(models.Model):
    _name = 'skill.setup'
    _rec_name='competency_name'
    competency_name = fields.Char(string="Skill Name", required=True, )
    mastery_level = fields.Selection(string="Mastery Level", selection=[('Level 1', 'Level 1'), ('Level 2', 'Level 2'),('Level 3', 'Level 3') ], required=True, )
    description = fields.Text()
class details(models.Model):
    _name = 'comp.details'
    details = fields.Text(string="Details", required=False, )
    competency_id = fields.Many2one(comodel_name="comp.learning", string="", required=False, )

