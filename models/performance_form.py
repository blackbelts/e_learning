# -*- coding: utf-8 -*-
from datetime import datetime
from odoo import models, fields, api

class e_learning(models.Model):
    _name = 'performance.form'
    _rec_name='app_id'
    app_id = fields.Text(string="Appraisal ID", required=False,compute='_compute_description',store=True )
    per = fields.Selection(string="Appraisal Per", selection=[('EOY', 'EOY'), ('EOQ', 'EOQ'), ], required=True , )
    quarter = fields.Selection(string="Quarter", selection=[('Q1', 'Q1'), ('Q2', 'Q2'),('Q3', 'Q3'),('Q4', 'Q4') ], required=False, )
    year = fields.Selection([(num, str(num)) for num in range(2018, (datetime.now().year)+5 )], 'Year', required=False)
    employee = fields.Many2one(comodel_name="hr.employee", string="Employee", required=False,domain="['|',('parent_id.user_id','=',uid),('user_id','=',uid)]", )
    department_name = fields.Many2one(comodel_name="hr.department", string="Department", required=False,
                                      related='employee.department_id', store=True)
    employee_id = fields.Integer(string="Employee ID", related='employee.id', store=True, required=False, )
    manager = fields.Many2one(comodel_name="hr.employee", string="1st Manager", required=False,
                              related='employee.parent_id', store=True)
    manager2 = fields.Many2one(comodel_name="hr.employee", string="2nd Manager", required=False,
                               related='manager.parent_id', store=True)
    emp_type = fields.Selection(string="Employee Type",
                                selection=[('Staff', 'Staff'), ('Middle', 'Middle'), ('Top', 'Top'), ], required=False,
                                related='manager.emp_type', store=True)
    targets_ids = fields.One2many(comodel_name="employee.targets", inverse_name="performance_id", string="", required=False,)
    competencies_ids = fields.One2many(comodel_name="employee.competencies", inverse_name="performance_id", string="", required=False, )
    comments_ids = fields.One2many(comodel_name="appraisal.comment", inverse_name="performance_id", string="", required=False, )
    state = fields.Selection([('Pending', 'To Submit'),
                              ('To Approve', 'To Approve'),
                              ('1st Approve', '1st Approval'),
                              ('2st Approve', '2st Approval'),
                              ], string='State', default='Pending',
                             group_expand='_expand_states',
                             track_visibility='onchange', help='Status of the contract', )
    target_weight = fields.Float(string="Target Weight %",  required=False,compute="get_section" )
    com_skill_weight = fields.Float(string="Comp/Skills Weight %",  required=False,compute="get_section" )
    skills_score = fields.Float(string="Skills Score %",  required=False,compute='get_skill_score',store=True )
    target_score = fields.Float(string="Targets Score %",  required=False,compute='get_target_score',store=True )
    final_score = fields.Float(string="Final Score %",  required=False,compute='get_final_score' )
    @api.one
    @api.depends('emp_type','year')
    def get_section(self):
        if self.emp_type:
            x = self.env['section.learning'].search(['&', ('type', '=', self.emp_type), ('year', '=', self.year),])
            self.target_weight=x.D_C
            self.com_skill_weight=x.JP_C

    @api.one
    @api.depends('year', 'per', 'quarter', 'employee')
    def _compute_description(self):
        self.app_id = (str(self.employee.name)if self.employee else " " + "_") + "-" +(
            str(self.year) if self.year else " " + "_") + "-" + (
            str(self.per) if self.per else " " + "_") + "-" + (
            str(self.quarter) if self.quarter else " " + "_")

    # @api.one
    @api.depends('competencies_ids')
    def get_skill_score(self):
        for x in self.competencies_ids:
            self.skills_score+=x.result
    @api.depends('targets_ids')
    def get_target_score(self):
        for x in self.targets_ids:
            self.target_score+=x.result
    @api.one
    @api.depends('target_score','skills_score','target_weight','com_skill_weight')
    def get_final_score(self):
        self.final_score=((self.target_score*self.target_weight)+(self.skills_score*self.com_skill_weight))/100

    @api.depends('employee')
    def get_employee_values(self):
        self.targets_ids.unlink()
        self.competencies_ids.unlink()
        # x=self.env['targets.details'].search(['|',('employee', '=', self.employee.id),('employee.parent_id', '=', self.employee.id)])
        x=self.env['target.learning'].search(['&','&',('employee', '=', self.employee.id),('year', '=', self.year),('per', '=', self.per)])

        print(x)
        for rec in x:
            for record in rec.targets_ids:
                self.update({
                'targets_ids': [(0, 0, {'name': record.target_name,
                                        'description':record.description,
                                        'weight': record.weight,
                                        'performance_id' : self.id,
                                      })], })
        y = self.env['comp.learning'].search(['|',('job_position', '=', self.employee.job_id.id),('job_position', '=', False)])
        print(y)
        for s in y:
            for rec in s.competencies_ids:
                self.update({
                'competencies_ids': [(0, 0, {
                                        'name': rec.skills_id.competency_name,
                                        'description': rec.skills_id.description,
                                        'mastery_level': rec.skills_id.mastery_level,
                                        'weight': rec.weight,
                                        'performance_id': self.id,
                                        })], })

class employeeTargets(models.Model):
    _name = 'employee.targets'
    _rec_name = 'name'

    name = fields.Char(readonly=True)
    manager_score = fields.Float(string="Manager Score %",  required=False, )
    employee_score = fields.Float(string="Employee Score %",  required=False, )
    description = fields.Text(readonly=True)
    weight  = fields.Float(string="Weight %",  required=False,readonly=True )
    result = fields.Float(string="Result %",  required=False,compute='get_result',store=True )
    comment = fields.Text(string="Comment", required=False, )
    performance_id = fields.Many2one(comodel_name="performance.form", string="", required=False, )
    @api.one
    @api.depends('weight', 'manager_score')
    def get_result(self):
        self.result = (self.weight * self.manager_score)/100
class employeeCompetencies(models.Model):
    _name = 'employee.competencies'
    _rec_name = 'name'
    name = fields.Char(readonly=True)
    manager_score = fields.Float(string="Manager Score %", required=False, )
    employee_score = fields.Float(string="Employee Score %", required=False, )
    description = fields.Text(readonly=True)
    weight = fields.Float(string="Weight %", required=False, readonly=True)
    result = fields.Float(string="Result %", required=False, compute='get_result', store=True)
    comment = fields.Text(string="Comment", required=False, )
    mastery_level = fields.Selection(string="Mastery Level",readonly=True, selection=[('Level 1', 'Level 1'), ('Level 2', 'Level 2'),('Level 3', 'Level 3') ], required=False, )
    performance_id = fields.Many2one(comodel_name="performance.form", string="", required=False, )
    @api.one
    @api.depends('weight', 'manager_score')
    def get_result(self):
        self.result = (self.weight * self.manager_score) / 100
class Comments(models.Model):
    _name = 'appraisal.comment'
    comment = fields.Text(string="Comment", required=False, )
    performance_id = fields.Many2one(comodel_name="performance.form", string="", required=False, )
