# -*- coding: utf-8 -*-
from datetime import datetime
from odoo import models, fields, api
from odoo.exceptions import ValidationError

class e_learning(models.Model):
    _name = 'target.learning'
    _rec_name='target_id'
    # name = fields.Many2one('performance.framework',string='Performance Framework',required=True, domain="[('framework_type','=','Target')]")
    target_id = fields.Char(string="Target ID", required=False,default=lambda self: self.env['ir.sequence'].next_by_code('target'),readonly=True )
    target_date = fields.Date(string="Date", required=False, default=datetime.today())
    year = fields.Selection([(num, str(num)) for num in range(2018, (datetime.now().year)+5 )], 'Year', required=True)
    employee = fields.Many2one(comodel_name="hr.employee", string="Employee", required=True,domain="[('parent_id.user_id','=',uid)]",)

    # target_type = fields.Selection(string="Target Level",
    #                                    selection=[('Company', 'Company'), ('Department', 'Department'),
    #                                               ('Job Position', 'Job Position')], required=True, )
    # freq = fields.Selection(string="Frequency", selection=[('EOY', 'EOY'), ('EOQ', 'EOQ'),('EOM', 'EOM') ], required=False, )
    # category = fields.Selection(string="Category", selection=[('Manager', 'Manager'), ('Staff', 'Staff'), ], required=False, )
    department_name = fields.Many2one(comodel_name="hr.department", string="Department", required=False,related='employee.department_id', store=True )
    employee_id = fields.Integer(string="Employee ID",related='employee.id', store=True, required=False, )
    manager = fields.Many2one(comodel_name="hr.employee", string="1st Manager", required=False,related='employee.parent_id', store=True)
    manager2 = fields.Many2one(comodel_name="hr.employee", string="2nd Manager", required=False,related='manager.parent_id', store=True)
    emp_type = fields.Selection(string="Employee Type", selection=[('Staff', 'Staff'), ('Middle', 'Middle'),('Top', 'Top'), ], required=False,related='manager.emp_type', store=True )
    per = fields.Selection(string="Target Per", selection=[('EOY', 'EOY'), ('EOQ', 'EOQ'), ], required=True , )
    targets_ids = fields.One2many(comodel_name="targets.details", inverse_name="target_id", string="", required=False, )
    state = fields.Selection(string="", selection=[('Pending', 'Pending'), ('Approved', 'Approved'), ], required=False, default='Pending',
                             group_expand='_expand_states',
                             track_visibility='onchange', help='Status of the contract',  )
    @api.constrains('employee')
    def _constrain_create(self):
        object = self.env['target.learning'].search(
            [('employee', '=', self.employee.id),
             ('year', '=', self.year), ('id', '!=', self.id)])
        if object:
            raise ValidationError('You Have Another target Setup for the same employee in this year!')

    @api.constrains('targets_ids')
    def _constrain_targets_ids(self):
        total=0
        for x in self.targets_ids:
            total+=x.weight
        if self.targets_ids and total != 100:
            raise ValidationError('Total Weights Must be equal 100 !')
    # weight  = fields.Float(string="Weight %",  required=False, )
class NewModule(models.Model):
    _name = 'targets.details'
    target_name = fields.Char(string="Target Name", required=False, )
    weight = fields.Float(string="Weight %", required=False, )
    # achieved = fields.Float(string="Achieved %", required=False, )
    target_id = fields.Many2one(comodel_name="target.learning", string="", required=False, )
    description = fields.Text()
    # valid_date = fields.Date(string="", required=False, related="target_id.valid_date")
    # manager = fields.Many2one(related="employee.parent_id" )
class emp_inherit(models.Model):
    _inherit = 'hr.employee'
    emp_type = fields.Selection(string="Employee Type", selection=[('Staff', 'Staff'), ('Middle', 'Middle'),('Top', 'Top'), ], required=False, )