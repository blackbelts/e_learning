# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError
from datetime import datetime



class sddf(models.Model):
    _name = 'section.learning'
    _rec_name='section_id'
    section_id = fields.Char(string="Section ID", required=False,default=lambda self: self.env['ir.sequence'].next_by_code('section'),readonly=True )
    # name = fields.Many2one('performance.framework',string='Competency Name',required=True, domain="[('framework_type','=','Section')]")
    # emp_target = fields.Float(string="",  required=False, )
    # valid_date = fields.Date(string="Valid Date", required=False, )
    D_C = fields.Float(string="Targets Section %",  required=False, )
    JP_C = fields.Float(string="Skills/Comp Section %",  required=False, )
    type = fields.Selection(string="Section Per", selection=[('Staff', 'Staff'), ('Middle', 'Middle'),('Top', 'Top') ], required=False, )
    # new_field = fields.Float(string="",  required=False, )
    year = fields.Selection([(num, str(num)) for num in range(2018, (datetime.now().year)+5 )], 'Year')
    total = fields.Float(string="Total",  required=False,readonly=True,compute= 'get_total',store=True)
    @api.depends('D_C','JP_C')
    def get_total(self):
        self.total=self.D_C+self.JP_C
    @api.constrains('type')
    def _constrain_create(self):
        object = self.env['section.learning'].search(
            [('type', '=', self.type),
             ('year', '=', self.year), ('id', '!=', self.id)])
        if object:
            raise ValidationError('You Have Another Section Setup in the same year and the same type !')

    @api.constrains('total')
    def _constrain_total(self):
        if self.total != 100:
            raise ValidationError('Total Must Be Equal 100 !')

